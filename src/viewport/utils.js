'use strict'
export const isInHorizontalViewport = (
  viewportOffset,
  viewportSize,
  elementOffset,
  elementSize,
  preTriggerRatio
) => {
  let inViewport = true
  const preTriggerAreaSize = preTriggerRatio
    ? preTriggerRatio * viewportSize
    : 0
  const elementEnd = elementOffset + elementSize
  const viewportEnd = viewportOffset + viewportSize
  const isViewportOffsetBeforeElement = viewportOffset <= elementOffset
  if (isViewportOffsetBeforeElement) {
    inViewport = elementOffset - preTriggerAreaSize <= viewportEnd
  } else {
    inViewport = elementEnd + preTriggerAreaSize >= viewportOffset
  }
  return inViewport
}
export const isInViewport = (
  viewportOffset,
  viewportSize,
  elementOffset,
  elementSize
) => {
  let inViewport = true;

  const APPBAR_HEIGHT = 60;

  const elementEnd = elementOffset + elementSize;
  const viewportEnd = viewportOffset + viewportSize;
  const isElementEndOffscreen = elementEnd > viewportEnd;

  const isElementLargerThanViewport = elementSize > (viewportSize - APPBAR_HEIGHT);
  const visibleAreaHalfwayPoint = ((viewportOffset + APPBAR_HEIGHT) + viewportSize * 0.5);

  const isViewportOffsetBeforeElement = viewportOffset + APPBAR_HEIGHT <= elementOffset;

  if (isViewportOffsetBeforeElement) {
    const isElementStartVisible = elementOffset >= (viewportOffset + APPBAR_HEIGHT)
    const isElementStartInTopHalf = isElementStartVisible && elementOffset <= visibleAreaHalfwayPoint;
    inViewport = isElementLargerThanViewport
      ? (isElementEndOffscreen) && isElementStartInTopHalf
      : elementEnd <= viewportEnd;
  } else {
    const isEndInBottomHalf = elementEnd >= visibleAreaHalfwayPoint;
    const isEndOnScreen = elementEnd <= viewportEnd;

    inViewport = isElementLargerThanViewport ?
      (isEndOnScreen && isEndInBottomHalf) || !isEndOnScreen
      : false;
  }
  return inViewport
}